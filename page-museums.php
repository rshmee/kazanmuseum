<?php
get_header();
?>

<main id="main-content">
    <header id="hero">
        <div class="container">
            <h1 class="hero-title">
                Kazan Museum Tour
            </h1>
            <h3>
                For you that love experience to Museum
            </h3>
        </div>
    </header>

    <section id="museums">
        <div class="container">
            <h1 class="section-title" style="text-align: center">
               All Museums
            </h1>
            <div class="content">
                <?php
                $museums = get_posts([
                    'post_type' => 'museum',
                    'numberposts' => -1
                ]);
                ?>

                <?php foreach($museums as $museum) : ?>

                    <div class="museum-wrapper">
                        <a href="<?= get_the_permalink($museum->ID) ?>">
                            <img src="<?= get_the_post_thumbnail_url($museum->ID) ?>" alt="">
                            <h3>
                                <?= $museum->post_title ?>
                            </h3>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
</main>

<?php
get_footer();
?>
