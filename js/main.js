const $ = jQuery;

$(document).ready(function(){
    $('#login h1 a').html('<h1>KMT</h1>');

    $('#content-wrapper').on('click', 'a', function(e){
        e.preventDefault();

        $('.preloader').addClass('active');

        let target = $(this).attr('href');

        $.ajax({
            url:target,
            success: function(res){
                $('body').html(res);
                $(window).scrollTop(0);
                history.pushState({}, '', target);
                $('.preloader').removeClass('active');
            }
        });
    })
});