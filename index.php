<?php
    get_header();
?>

<main id="main-content">
    <header id="hero">
        <div class="container">
            <h1 class="hero-title">
                Kazan Museum Tour
            </h1>
            <h3>
                For you that love experience to Museum
            </h3>
        </div>
    </header>

    <section id="museums">
        <div class="container">
            <h1 class="section-title">
               Museums
            </h1>
            <div class="content">
               <?php
                    $museums = get_posts([
                            'post_type' => 'museum',
                            'numberposts' => 5
                    ]);
               ?>

                <?php foreach($museums as $museum) : ?>

                    <div class="museum-wrapper">
                        <a href="<?= get_the_permalink($museum->ID) ?>">
                            <img src="<?= get_the_post_thumbnail_url($museum->ID) ?>" alt="">
                            <h3>
                                <?= $museum->post_title ?>
                            </h3>
                        </a>
                    </div>

                <?php endforeach; ?>
            </div>
            <div style="display: flex;">
                <a href="<?= site_url('/museums') ?>" class="btn-link">
                    All Museums
                </a>
            </div>
        </div>
    </section>

    <section id="content">
        <div class="container flex-2">
            <div id="news">
                <h1 class="section-title">
                    News
                </h1>
                <ul>
                    <?php
                        $news = get_posts([
                                'post_type' => 'post',
                                'numberposts' => -1,
                                'orderby' => 'date',
                                'order' => 'desc'
                        ]);
                    ?>

                    <?php foreach($news as $new) : ?>

                        <li>
                            <img src="<?= get_the_post_thumbnail_url($new->ID) ?>" alt="">
                            <h3 class="news-title">
                                <?= $new->post_title ?>
                            </h3>
                            <p>
                                <?= substr($new->post_content, 0, 100) ?>
                            </p>
                            <a href="<?= get_the_permalink($new->ID) ?>" class="more">
                               Read More
                            </a>
                        </li>

                    <?php endforeach; ?>
                </ul>

                <div style="display: flex;">
                    <a href="<?= site_url('/news') ?>" class="btn-link">
                        All News
                    </a>
                </div>
            </div>

            <div id="cover">
                <h1 class="section-title">
                    Gallery
                </h1>
                <div class="cover-wrapper">
                    <?php
                    $images = get_posts([
                        'post_type' => 'attachment',
                        'numberposts' => -1
                    ]);
                    ?>

                    <?php foreach($images as $cover) : ?>

                        <img src="<?= wp_get_attachment_url($cover->ID) ?>" alt="">

                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>
    
    <section id="contact">
        <div class="container">
            <h1 class="section-title" style="color:#fff">
                Contact Us
            </h1>
           <div class="flex-2">
               <img src="<?= get_template_directory_uri() . './assets/images/bg-contact.jpg' ?>" alt="" class="img-contact">
               <div class="content">
                   <?= do_shortcode('[contact-form-7 id="34" title="Contact form 1"]') ?>
               </div>
           </div>
        </div>
    </section>
</main>

<?php
    get_footer();
?>
