<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width" />
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div class="preloader">
    <div class="content">
        Loading...
    </div>
</div>

<div id="content-wrapper">

<nav id="navbar">
    <a href="<?= site_url('/') ?>" class="brand">Kazan MuseumTour</a>
    <ul class="navbar-ul">
        <li class="nav-item">
            <a href="<?= site_url('/') ?>">Home</a>
        </li>
        <li class="nav-item">
            Museums

            <ul class="dropdown">
                <?php

                $museums = get_posts([
                    'post_type' => 'museum',
                    'numberposts' => 5,
                    'orderby' => 'name',
                    'order' => 'asc'
                ]);

                ?>

                <?php foreach($museums as $museum) : ?>

                    <li>
                        <a href="<?= get_the_permalink($museum->ID) ?>">
                            <?= $museum->post_title ?>
                        </a>
                    </li>

                <?php endforeach; ?>
            </ul>
        </li>
        <li class="nav-item">
            Seasoned Events

            <ul class="dropdown">
                <?php

                $events = get_posts([
                    'post_type' => 'post',
                    'numberposts' => 5,
                    'category' => 3,
                    'orderby' => 'name',
                    'order' => 'asc'
                ]);

                ?>

                <?php foreach($events as $event) : ?>

                    <li>
                        <a href="<?= get_the_permalink($event->ID) ?>">
                            <?= $event->post_title ?>
                        </a>
                    </li>

                <?php endforeach; ?>
            </ul>
        </li>
    </ul>

    <div id="bars">
        <div class="bar"></div>
        <div class="bar"></div>
        <div class="bar"></div>
    </div>
</nav>