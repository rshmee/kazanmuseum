</div>
<footer id="footer">
    <div id="copyright">
        Copyright &copy; <?= date('Y') ?> - All rights reserved
    </div>
    <div id="socmeds">
        <ul>
           <?php
                $socmeds = get_posts([
                        'post_type' => 'socmeds',
                        'numberposts' => -1,
                        'orderby' => 'name',
                        'order' => 'desc'
                ]);
           ?>

            <?php foreach($socmeds as $socmed) : ?>

                <li>
                    <a href="<?= get_field('link', $socmed->ID) ?>" target="_blank">
                        <?= $socmed->post_title ?>
                    </a>
                </li>

            <?php endforeach; ?>
        </ul>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>