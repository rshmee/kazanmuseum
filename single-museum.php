<?php
get_header();
?>

<main id="main-content">
    <header id="hero" style="
        <?php
        if(get_field('selected')){
            ?>
            background: linear-gradient(var(--secondary), var(--primary)), url(<?= get_the_post_thumbnail_url($post->ID) ?>);
            background-blend-mode: overlay;
            background-size:cover;
            background-position:center center;
            background-repeat: no-repeat;
            width:100%;
            height:100vh;
            position:relative;
            <?php
        }
        else{
            ?>
                background: linear-gradient(var(--secondary), var(--primary));
                background-blend-mode: overlay;
                background-size:cover;
                background-position:center center;
                background-repeat: no-repeat;
                width:100%;
                height:40vh;
                position:relative;
            <?php
        }
        ?>
    ">
        <div class="container">
            <h1 class="hero-title">
               <?= $post->post_title ?>
            </h1>
        </div>
    </header>

    <section id="single-museum" style="min-height: 200px;">
        <div class="container">
            <h1 class="section-title" style="text-align: center">
                <?= $post->post_title ?>
            </h1>
            <p>
                <?= $post->post_content ?>
            </p>
        </div>
    </section>

    <?php
        if(get_field('selected')){

            $news = get_posts([
                'post_type' => 'post',
                'numberposts' => -1,
                'tax_query' => [
                    [
                        'taxonomy' => 'category',
                        'field' => 'slug',
                        'terms' => $post->post_name
                    ]
                ]
            ]);

            ?>

            <section id="page-news">
                <div class="container">
                    <h1 class="section-title">
                        News
                    </h1>
                    <div class="content">
                        <?php foreach($news as $new) : ?>
                            <div class="news-wrapper">
                                <img src="<?= get_the_post_thumbnail_url($new->ID) ?>" alt="">
                                <h3>
                                    <?= $new->post_title ?>
                                </h3>
                                <p>
                                    <?= substr($new->post_content, 0, 200) ?>
                                </p>
                                <a href="<?= get_the_permalink($new->ID) ?>" class="more">
                                    Read More
                                </a>
                            </div>

                        <?php endforeach; ?>
                    </div>
                </div>
            </section>

            <?php

        }
    ?>

</main>

<?php
get_footer();
?>
