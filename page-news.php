<?php
get_header();
?>

<main id="main-content">
    <header id="hero">
        <div class="container">
            <h1 class="hero-title">
                News
            </h1>
            <h3>
                Lets read our news
            </h3>
        </div>
    </header>

    <section id="page-news">
        <div class="container">
            <h1 class="section-title" style="text-align: center">
                All News
            </h1>
            <div class="content">
                <?php
                $news = get_posts([
                    'post_type' => 'post',
                    'numberposts' => -1
                ]);
                ?>

                <?php foreach($news as $new) : ?>

                    <div class="news-wrapper">
                        <img src="<?= get_the_post_thumbnail_url($new->ID) ?>" alt="">
                        <h3>
                            <?= $new->post_title ?>
                        </h3>
                        <p>
                            <?= substr($new->post_content, 0, 200) ?>
                        </p>
                        <a href="<?= get_the_permalink($new->ID) ?>" class="more">
                            Read More
                        </a>
                    </div>

                <?php endforeach; ?>
            </div>
        </div>
    </section>
</main>

<?php
get_footer();
?>
