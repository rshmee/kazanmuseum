<?php
get_header();
?>

<main id="main-content">
    <header id="hero" style="
            background: linear-gradient(var(--secondary), var(--primary)), url(<?= get_the_post_thumbnail_url($post->ID) ?>);
            background-blend-mode: overlay;
            background-size:cover;
            background-position:center center;
            background-repeat: no-repeat;
            width:100%;
            height:100vh;
            position:relative;
            ">
        <div class="container">
            <h1 class="hero-title">
                <?= $post->post_title ?>
            </h1>
        </div>
    </header>

    <section id="single-news" style="min-height: 200px;">
        <div class="container">
            <h1 class="section-title" style="text-align: center">
                <?= $post->post_title ?>
            </h1>
            <p>
                <?= $post->post_content ?>
            </p>
        </div>
    </section>
</main>

<?php
get_footer();
?>
